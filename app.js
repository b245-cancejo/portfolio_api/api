const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;
const bodyparser = require('body-parser');
const cors = require('cors');
const nodemailer = require('nodemailer');
const sendGridTransporter = require('nodemailer-sendgrid-transport');

require('dotenv').config();
app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));


const options = {
    auth: { 
        api_key: process.env.API_SENDGRID
    }
};
const mailer = nodemailer.createTransport(sendGridTransporter(options));

app.post("/sendEmail", (request, response) => {
    const {name, email, jobtypes, message} = request.body;

    if(!name){
        return response.status(402).json({error: "Please add your name"})
    }
    if(!email){
        return response.status(402).json({error: "Please add your email"})
    }
    if(!jobtypes){
        return response.status(402).json({error: "Please add jobtypes"})
    }
    if(!message){
        return response.status(402).json({error: "Please add your message"})
    }
    const mail = {
        to: 'trylungs123@gmail.com',
        from: 'portfolio.api1007@gmail.com',
        subject: "Job Offer",
        html: `
            <h5>Details Information:</h5>  

            <ul>
            <li> <p>Name: ${name} </p> </li>
            <li> <p>E-mail: ${email} </p> </li>
            <li> <p>Jobtypes: ${jobtypes} </p> </li>
            <li> <p>Message: ${message} </p> </li>
            </ul>
        `
    }
    mailer.sendMail(mail)
    response.json({success: "Your e-mail has been sent"})
});

app.listen(PORT, (request, response) => {
    console.log("Server Connected Successfully!")
});

